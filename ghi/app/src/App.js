import Nav from './Nav';
import AttendeesList from './AttendeeList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';




function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="/locations/new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="/conferences/new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees" >
            <Route path="/attendees/new" element={<AttendConferenceForm />} />
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
          </Route>
          <Route path="presentations">
            <Route path="/presentations/new" element={<PresentationForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
