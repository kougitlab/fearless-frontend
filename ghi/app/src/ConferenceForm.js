

import React, { useEffect, useState } from 'react';

const ConferenceForm = props => {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMaximumPresentations] = useState('');
    const [max_attendees, setMaximumAttendees] = useState('');
    const [location, setLocation] = useState('');
    

    

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }
    const handleStartschange = (event) => {
        const value = event.target.value
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value);
    }
    const handlelocationChange = (event) => {
        const value = event.target.value
        setLocation(value);
    }
    const handleMaximumPresentationsChange = (event) => {
        const value = event.target.value
        setMaximumPresentations(value);
    }
    const handleMaximumAttendeesChange = (event) => {
        const value = event.target.value
        setMaximumAttendees(value);
    }

    const resetState = () => {
        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setLocation('');
        setMaximumPresentations('');
        setMaximumAttendees('');
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;


        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference)
          resetState();
        }
    }
    
    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
        
      setLocations(data.locations)
      
      }
    }
  

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" className="form-control" name="name"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStartschange} value={starts} placeholder="starts" required type="date" id="starts" className="form-control" name="starts"/>
                <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="ends" required type="date" id="ends" className="form-control" name="ends"/>
                <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3 form-group">
                <label  htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleMaximumPresentationsChange} value={max_presentations} placeholder="max_presentations" required type="number" id="max_presentations" className="form-control" name="max_presentations"/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaximumAttendeesChange} value={max_attendees} placeholder="max_attendees" required type="number" id="max_attendees" className="form-control" name="max_attendees"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handlelocationChange} value={location} required id="location" className="form-select" name="location">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>
  )
}

export default ConferenceForm;

